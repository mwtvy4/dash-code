/*********************************************************************
This is a library for our Monochrome OLEDs based on SSD1325 drivers

  Pick one up today in the adafruit shop!
  ------> http://www.adafruit.com/category/63_98

These displays use SPI to communicate, 4 or 5 pins are required to  
interface

Adafruit invests time and resources providing this open source code, 
please support Adafruit and open-source hardware by purchasing 
products from Adafruit!

Written by Limor Fried/Ladyada  for Adafruit Industries.  
BSD license, check license.txt for more information
All text above, and the splash screen below must be included in any redistribution
*********************************************************************/

#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1325.h>
#include <FastLED.h>
#include <Wire.h>
#include <TLC59116.h>
#include <CAN.h>
#include <Button.h>

float gIN = 0; // test input
bool up = true; // counting up or not
char consoleMessage[5][27];//5 1 line arrays
int consoleRotate = 0; //console timer
int consoleIndex = 0; //number in the console message array

#define lcdBrite  255
#define ledBrite 30

//0x20
uint16_t throttleOneRaw = 0;
float throttleOneF = 0;
uint16_t throttleTwoRaw = 0;
float throttleTwoF = 0;
uint16_t brakePressureFrontRaw = 0;
float brakePressureFrontF = 0;
uint16_t brakePressureRearRaw = 0;
float brakePressureRearF = 0;
bool brakePressureBool = false;
float brakeMax = 0;
bool throttlePlaus = false;
float throttleDiff = 0;

//0x28
bool IMDfault = false;//statuses
bool AMSfault = false;
bool driveActive = false;
bool startActive = false;
bool shutdownActive = false;
uint16_t LVSOC = 0; //Low Voltage State Of Charge
float LVSOCF = 0; //float version for gauge readings
int carSpeed = 0; // car speed read through CAN bus 
double carSpeedF = 0; // float
bool driveForward; // motor direction
int nannyMode = 0; // for normal, or limited modes

//0x29
uint16_t MCTemp = 0;
float MCTempF = 0;
uint16_t motorTemp = 0;
float motorTempF = 0;
uint16_t batteryTemp = 0;
float batteryTempF = 0;

//0x41
uint16_t maxCellTemp = 0;
float maxCellTempF = 0;
uint16_t HVSOC = 0; //High Voltage State Of Charge
float HVSOCF = 0; //float for gauges

//0x42
uint16_t packVoltage = 0;
float packVoltageF = 0;
uint16_t maxCellVoltage = 0;
float maxCellVoltageF = 0;
uint16_t minCellVoltage = 0;
float minCellVoltageF = 0;

//0x43
uint16_t packCurrent = 0;
float packCurrentF = 0;


uint8_t dash = 1;//start on dash 1
uint8_t dashCount = 5;//total number of dashes


//Button setup
Button bottomRightButton = Button(0,PULLUP);
int BRBPin = 0;
Button topRightButton = Button(1,PULLUP);
int TRBPin = 1;
Button bottomLeftButton = Button(20,PULLUP);
int BLBPin = 20;
Button topLeftButton = Button(21,PULLUP);
int TLBPin = 21;


//CAN setup
const int canSpeed = 500;
const int canInterruptPin = 23;
const int canResetPin = 22;
CANBus CAN = CANBus(10);


//LED setup
#define NUM_LEDS 4
#define DATA_PIN 2
#define CLOCK_PIN 3
CRGB leds[NUM_LEDS];


//Seven Segment setup
TLC59116 sevenSeg(0);
const uint8_t pinmap[8] = { //pinmap, A-G in order
    15 << 4 | 0,
    14 << 4 | 1,
    13 << 4 | 2,
    12 << 4 | 3,
    11 << 4 | 4,
    10 << 4 | 5,
    9 << 4 | 6,
    8 << 4 | 7
};


//OLED display setup
// If using software SPI, define CLK and MOSI
#define OLED_CLK 13
#define OLED_MOSI 11
// These are neede for both hardware & software SPI
#define OLED_CS 9
#define OLED_RESET 15
#define OLED_DC 14
//Adafruit_SSD1325 OLED(OLED_MOSI, OLED_CLK, OLED_DC, OLED_RESET, OLED_CS);
// this is for hardware SPI, fast! but fixed oubs
Adafruit_SSD1325 OLED(OLED_DC, OLED_RESET, OLED_CS);


//kitt setup
uint8_t kittint = 0;
bool kittup = true;

const unsigned char LVbmp [] PROGMEM = {
0x90, 0x66, 0x66, };

const unsigned char LVbig [] PROGMEM = {
0xff, 0xfe, 0x3f, 0xd7, 0xf0, 0x7e, 0xef, 0xdd, 0xfb, 0xbf, 0x77, 0xee, 0xfd, 0xdf, 0x83, 
};

const unsigned char LVman [] PROGMEM = {
B00000011,B10000000,
B00000010,B10000000,
B00001100,B01100000,
B00001000,B00100000,
B00001000,B00100000,
B00001000,B00100000,
B00001000,B00100000,
B00001000,B00100000,
B00001000,B00100000,
B00001000,B00100000,
B00001000,B00100000,
B00001111,B11100000,  
};


const unsigned char tempbmp [] PROGMEM = {
0x04, 0x00, 0x07, 0x00, 0x04, 0x00, 0x07, 0x00, 0x04, 0x00, 0x07, 0x00, 0x04, 0x00, 0x0e, 0x00, 
0x04, 0x00, 0x22, 0x00, 0x55, 0x40, 0x08, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 

};

const unsigned char HVbmp [] PROGMEM = {
 0x0e, 0x00, 0x0a, 0x00, 0x11, 0x00, 0x11, 0x00, 0x24, 0x80, 0x28, 0x80, 0x44, 0x40, 0x42, 0x40, 
0x84, 0x20, 0x80, 0x20, 0x7f, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 
};

const unsigned char STlogo [] PROGMEM = {
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0xE0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3C, 0xF8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x80, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00, 0x01, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x0C, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x1E, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x7F, 0xA1, 0xF8, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0xFC, 0xC3, 0xEC, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0xF8, 0x87, 0xC4, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0xF3, 0x87, 0x84, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x7E, 0x06, 0x18, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x42, 0x33, 0xE0, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x43, 0xC0, 0x00, 0x80, 0x00, 0x07, 0xC0, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0x07, 0xF8, 0x80, 0x00, 0x1F, 0xE0, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x80, 0x00, 0x3F, 0xD8, 0x1C, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x80, 0x00, 0x63, 0xC7, 0x7E, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x80, 0x00, 0x61, 0x27, 0x9E, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x80, 0x00, 0xC1, 0x37, 0x9E, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x80, 0x00, 0xC1, 0xFF, 0x9A, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x80, 0x00, 0x81, 0xFC, 0x62, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x28, 0xC0, 0x01, 0x83, 0xDC, 0x62, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x28, 0x40, 0x03, 0x03, 0xC7, 0xE6, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x28, 0x41, 0xFF, 0x06, 0x47, 0x1E, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x28, 0x6E, 0x0E, 0x0C, 0x3F, 0x1E, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0x00, 0x28, 0x30, 0x0C, 0x18, 0x7F, 0x3A, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x28, 0x23, 0x18, 0x1F, 0xF1, 0xC2, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x28, 0x2D, 0xFF, 0x80, 0xF9, 0xC2, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x60, 0x00, 0x28, 0x03, 0x00, 0xE7, 0xC7, 0xE6, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x00, 0x28, 0x04, 0x00, 0x31, 0xE0, 0xFC, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x00, 0x28, 0x00, 0x00, 0x10, 0x30, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x00, 0x66, 0x00, 0x00, 0x18, 0x70, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x00, 0xC1, 0x00, 0x00, 0x0F, 0xE0, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x00, 0x84, 0x80, 0x00, 0x0F, 0x80, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x0C, 0x01, 0x6B, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x01, 0xB8, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x80, 0x00, 0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0x00, 0x00, 0x00, 0xE0, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, 0x00, 0x00, 0x01, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3C, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0F, 0xC0, 0x01, 0xFE, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0xFF, 0xF9, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x80, 0x09, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x05, 0x80, 0x09, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x8D, 0x83, 0xF9, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x69, 0x01, 0x19, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x3B, 0x00, 0xC1, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x82, 0x00, 0x61, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC6, 0x00, 0x39, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3C, 0x00, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };


void setup()   {
    //Serial.begin(9600);
  //Serial.println("Serial OK");


//Setup OLED
  OLED.begin();
  OLED.setTextSize(1);
  OLED.setTextColor(WHITE);

 
//Setup CAN
  pinMode(canResetPin, OUTPUT);
  digitalWrite(canResetPin, HIGH);
  pinMode(canInterruptPin, INPUT);
  SPI.usingInterrupt(canInterruptPin);
  attachInterrupt(digitalPinToInterrupt(canInterruptPin), CanRXISR, FALLING);

   
  CAN.Begin(canSpeed, CONFIGURATION ,false, 0x03);
//  CAN.BitModify(CAN.CANINTF, 0xFF, 0x00);
//          CAN.SetMask(0, CAN.StdIDToExt(0x000));
        //  CAN.SetMask(1, 0xFFFFFFFF);
         // CAN.SetMask(1, 0xFFFFFFF0);
        //  CAN.SetFilter(1, false, 0x20);
          
       //   CAN.SetMask(2, 0xFFFFFFFF);
       //   CAN.SetFilter(2, false, 0x20);
          
          
//          CAN.SetMask(0, 0x000);
//          CAN.SetMask(1, 0x000);
//          CAN.SetFilter(0,false, 0x000);
//          
          //        CAN.SetMask(1, CAN_INT0.StdIDToExt(0x1ff));
//        CAN.SetFilter(1, false, CAN_INT0.StdIDToExt(0x625));
//        CAN.SetFilter(2, false, CAN_INT0.StdIDToExt(0x626));
//        CAN.SetFilter(3, false, CAN_INT0.StdIDToExt(0x1ff));
//        CAN.SetFilter(4, false, CAN_INT0.StdIDToExt(0x1ff));
//        CAN.SetFilter(5, false, CAN_INT0.StdIDToExt(0x1ff));
  CAN.SetMode(NORMAL);
//  CAN.SetMode(LOOPBACK);


//Button interrupt setup
  SPI.usingInterrupt(BLBPin);
  attachInterrupt(digitalPinToInterrupt(BLBPin), dashSwitchBottom, FALLING);
  SPI.usingInterrupt(TLBPin);
  attachInterrupt(digitalPinToInterrupt(TLBPin), dashSwitchTop, FALLING);
//  SPI.usingInterrupt(TRBPin);
//  attachInterrupt(digitalPinToInterrupt(TRBPin), frameHandler, FALLING);
//  SPI.usingInterrupt(BRBPin);
//  attachInterrupt(digitalPinToInterrupt(BRBPin), frameHandler, FALLING);

//Setup LED
  FastLED.addLeds<APA102, DATA_PIN, CLOCK_PIN>(leds, NUM_LEDS);
  FastLED.setBrightness(ledBrite); //Turned down to save your eyes

  
//Setup 7 segment pinmap
  sevenSeg.begin();
  sevenSeg.setPinMapping(pinmap);
  

//Build Console Array
 for (int i=0;i<5;i++){
  strcpy(consoleMessage[i],"");
 }
// strcpy(consoleMessage[2],"Static Message");

 
///
// Systems check
///

//OLED.display(); // show splashscreen

// test bitmap
OLED.display();

//test all colors of LED
lightWRGB(250);

// test all segments
sevenUp(50,255);

delay(500);//pause to read

clearLED();
sevenClear();
OLED.clearDisplay();
}


//
//  Main loop
//

void loop() {
  
OLED.clearDisplay();

//  // gIN test input
//  if (gIN >= 99)up=false;
//  if (gIN <= 0) up=true; 
//  if (up == true) gIN++;
//  else gIN--;
 
 switch(dash){
    case 1://Lamborghini style
      lamboGauge(carSpeedF);
      lamboArrowRight(HVSOCF);
      lamboArrowLeft(motorTempF);
      sevenSeg.displayNumber(HVSOCF,lcdBrite);
      OLED.drawBitmap(98, 48,  HVbmp , 16, 16, WHITE);
      OLED.drawBitmap(20, 48,  tempbmp, 16, 12, WHITE);
    break;
    
    case 2: //space shuttle style
    
      shuttleTemp(throttleOneF,throttleTwoF,brakePressureFrontF/1.5);
      shuttleThrottle(throttleOneF);
      shuttleVolt(HVSOCF,LVSOCF-13/.035,minCellVoltageF);
      sevenSeg.displayNumber(HVSOCF,lcdBrite);
      OLED.setCursor(30,24);
      OLED.print("Drive Mode");
    break;
    
    case 3: //space shuttle battery monitor
    
      shuttleTemp2(MCTempF,motorTempF,batteryTempF);
      //shuttleThrottle(packCurrent);
      hTMGauge(26,32,102,25,packCurrentF/2);
      shuttleVolt2(packVoltageF,maxCellVoltageF,minCellVoltageF);
      sevenSeg.displayNumber(HVSOCF,lcdBrite);
      OLED.setCursor(40,24);
      OLED.print("Monitor Mode");
      
    break;

    case 4: //muscle car style
      galaxyGauge(carSpeedF);
//      sevenSeg.displayNumber(HVSOCF,128);
      sevenClear();
    break;

    case 5: //zen mode
      sevenClear();
    break;  
  }
 

  //always do these
  displayConsole();
  led0(0,0,startActive);
  led1(0,0,AMSfault);
  led2(0,0,IMDfault);
  //led3(0,0,driveActive);
  
  OLED.display();
  FastLED.show();
  frameHandler();
  //while (throttlePlaus == true){gameOver();sevenClear();}
  //if (throttleDiff >= 10){ sprintf(consoleMessage[3],"%f",throttleDiff);}
  //sprintf(consoleMessage[0],"MCtemp %3.1f",MCTempF);
  //sprintf(consoleMessage[1],"Motor %3.1f",motorTempF);
  //sprintf(consoleMessage[2],"Batt %3.1f",batteryTempF);
}

//End main loop
  




///abandon hope all ye who enter here 








void clearLED(void){
   leds[0] = CRGB::Black;
   leds[1] = CRGB::Black;
   leds[2] = CRGB::Black; 
   leds[3] = CRGB::Black;
   FastLED.show();
}


void lightWRGB(int wait){
 
  leds[0] = CRGB::White;
  leds[1] = CRGB::Red;
  leds[2] = CRGB::Green;
  leds[3] = CRGB::Blue;
  FastLED.show();
  delay(wait);
  leds[0] = CRGB::Blue;
  leds[1] = CRGB::White;
  leds[2] = CRGB::Red;
  leds[3] = CRGB::Green;
  FastLED.show();
  delay(wait);
  leds[0] = CRGB::Green;
  leds[1] = CRGB::Blue;
  leds[2] = CRGB::White;
  leds[3] = CRGB::Red;
  FastLED.show();
  delay(wait);
  leds[0] = CRGB::Red;
  leds[1] = CRGB::Green;
  leds[2] = CRGB::Blue;
  leds[3] = CRGB::White;
  FastLED.show();
  delay(wait);
}


void spiralLight(uint8_t count,uint8_t wait,CRGB color) {
   for (int i=0;i<count;i++){
   leds[0] = color;
//   leds[1] = CRGB::Black;
//   leds[2] = CRGB::Black; 
//   leds[3] = CRGB::Black;
   FastLED.show();
   delay(wait);

//   leds[0] = CRGB::Black;
   leds[1] = color;
//   leds[2] = CRGB::Black; 
//   leds[3] = CRGB::Black;
   FastLED.show();
   delay(wait);

//   leds[0] = CRGB::Black;
//   leds[1] = CRGB::Black;
   leds[2] = color; 
//   leds[3] = CRGB::Black;
   FastLED.show();
   delay(wait);   

//   leds[0] = CRGB::Black;
//   leds[1] = CRGB::Black;
//   leds[2] = CRGB::Black; 
   leds[3] = color;
   FastLED.show();
   delay(wait);
   }
}


  // Horizontal thermometer type gauge. Use a float input as a percentage of the thermometer reading
   void hTGauge(uint8_t xPos,uint8_t yPos, uint8_t w, uint8_t h, float input){
     OLED.drawRect(xPos,yPos,w,h,WHITE);
     OLED.fillRect(xPos,yPos,input*w/100,h,WHITE);
   }

   
  // Horizontal Mirrored
   void hTMGauge(uint8_t xPos,uint8_t yPos, uint8_t w, uint8_t h, float input){
     OLED.drawRect(xPos,yPos,w,h,WHITE);
     OLED.fillRect(xPos+(w-(input*w/100)),yPos,w-(w-(input*w/100)),h,WHITE);
   }


   // Vertical thermometer 
   void vTGauge(uint8_t xPos,uint8_t yPos, uint8_t w, uint8_t h, float input) {
     OLED.drawRect(xPos,yPos,w,h,WHITE);
     OLED.fillRect(xPos,yPos,w,input*h/100,WHITE);
   }

   
   // Vertical Mirrored
   void vTMGauge(uint8_t xPos,uint8_t yPos, uint8_t w, uint8_t h, float input){
     OLED.drawRect(xPos,yPos,w,h,WHITE);
     OLED.fillRect(xPos,yPos+(h-(input*h/100)),w,h-(h-(input*h/100)),WHITE);
   }
   
   
  


   //Round Gauge w/ warning
   //NOTE: The readout looks good in the demo, but shouldn't be implemented this way (use separate text for proper numerical output instead of %)
   void gauge(uint8_t xPos, uint8_t yPos, uint8_t r, float percentage){
    float input = (percentage / 100 * 4.71) - .78;
    OLED.setCursor(xPos-5,yPos+6);
    OLED.setTextSize(1);
    if (percentage > 60){
      OLED.fillCircle(xPos,yPos,r,WHITE);
      OLED.drawLine(xPos,yPos,cos(input)*-r + xPos,sin(input)*-r + yPos,BLACK);
      OLED.setTextColor(BLACK);
      OLED.print(int(percentage));
      }
   else{ 
      OLED.drawCircle(xPos,yPos,r,WHITE);
      OLED.drawLine(xPos,yPos,cos(input)*-r + xPos,sin(input)*-r + yPos,WHITE);
      OLED.setTextColor(WHITE);
      OLED.print(int(percentage));
      }   
   }


      //inverted
      //NOTE: The readout looks good in the demo, but shouldn't be implemented this way (use separate text for proper numerical output instead of %)
   void invgauge(uint8_t xPos, uint8_t yPos, uint8_t r, float percentage){
    float input = (percentage / 100 * 6.28) - 0; // originally 4.71 - .78
    int arrowLength = 2;
    float angle = 1.57;
     
    OLED.setCursor(xPos-10,yPos-20);
    OLED.setTextSize(2);
    if (percentage > 60){
      OLED.fillCircle(xPos,yPos,r,WHITE);
      OLED.drawLine(xPos,yPos,cos(input)*-r + xPos,-1*sin(input)*-r + yPos,BLACK);
      OLED.fillTriangle(-cos(input)*r+xPos,sin(input)*r+yPos,-cos(input+angle)*arrowLength+xPos,sin(input+angle)*arrowLength+yPos,-cos(input-angle)*arrowLength+xPos,sin(input-angle)*arrowLength+yPos,BLACK);
      OLED.fillTriangle(-cos(input+3.14)*arrowLength*2+xPos,sin(input+3.14)*arrowLength*2+yPos,-cos(input+angle+3.14)*arrowLength+xPos,sin(input+angle+3.14)*arrowLength+yPos,-cos(input-angle+3.14)*arrowLength+xPos,sin(input-angle+3.14)*arrowLength+yPos,BLACK);
      OLED.setTextColor(BLACK);
      OLED.print(int(percentage));
      }
   else{ 
      OLED.drawCircle(xPos,yPos,r,WHITE);
      OLED.drawLine(xPos,yPos,cos(input)*-r + xPos,-1*sin(input)*-r + yPos,WHITE);
      OLED.fillTriangle(-cos(input)*r+xPos,sin(input)*r+yPos,-cos(input+angle)*arrowLength+xPos,sin(input+angle)*arrowLength+yPos,-cos(input-angle)*arrowLength+xPos,sin(input-angle)*arrowLength+yPos,WHITE);
      OLED.fillTriangle(-cos(input+3.14)*arrowLength*2+xPos,sin(input+3.14)*arrowLength*2+yPos,-cos(input+angle+3.14)*arrowLength+xPos,sin(input+angle+3.14)*arrowLength+yPos,-cos(input-angle+3.14)*arrowLength+xPos,sin(input-angle+3.14)*arrowLength+yPos,WHITE);
      OLED.setTextColor(WHITE);
      OLED.print(int(percentage));
      }
   }

   
   //Indicator Light
   void idiotLite(uint8_t xPos, uint8_t yPos, uint8_t r, bool on){
    if (on == true) OLED.fillCircle(xPos, yPos, r, WHITE);
    else OLED.drawCircle(xPos, yPos, r, WHITE);
   }

   
   //Indicator Light
   void idiotLiteShuttle(uint8_t xPos, uint8_t yPos, uint8_t r, bool on){
    if (on == true) OLED.fillCircle(xPos, yPos, r, BLACK);
   // else OLED.drawCircle(xPos, yPos, r, WHITE);
   }


   //Seven setup test
   void sevenUp(uint8_t wait,uint8_t brite){
    for (uint8_t i=0;i<16;i++){
      sevenSeg.analogWrite(i,brite);
      delay(wait);
    }
   }


   void sevenClear() {
    sevenSeg.displayNumber(69,0);
   }


    void SOCmeter(uint8_t xPos,uint8_t yPos,uint8_t w,uint8_t h,float soc,char label){
    float percent = soc/100;
    OLED.drawRect(xPos+24,yPos,w,h,WHITE);
    OLED.fillRect(xPos+24,yPos,w*percent,h,WHITE);
    OLED.setCursor((xPos),yPos);
    OLED.setTextColor(WHITE);
    OLED.setTextSize(1);
    char message[5] = "00%H";
    sprintf(message,"%2.0f%%%c",soc,label);
    OLED.print(message);
   }


   void voltmeter(uint8_t xPos,uint8_t yPos,uint8_t w,uint8_t h,float volts){
    float input = (volts - 3) / 1.2;
    OLED.drawRect(xPos+24,yPos,w,h,WHITE);
    OLED.fillRect(xPos+24,yPos,w*input,h,WHITE);
    OLED.setCursor((xPos),yPos);
    OLED.setTextColor(WHITE);
    OLED.setTextSize(1);
    char message[5] = "0.0V";
    sprintf(message,"%1.1fV",volts);
    OLED.print(message);
   }
  void voltmeter2(uint8_t xPos,uint8_t yPos,uint8_t w,uint8_t h,float volts){
    float input = (volts)/300;
    OLED.drawRect(xPos+24,yPos,w,h,WHITE);
    OLED.fillRect(xPos+24,yPos,w*input,h,WHITE);
    OLED.setCursor((xPos),yPos);
    OLED.setTextColor(WHITE);
    OLED.setTextSize(1);
    char message[5] = "0.0V";
    sprintf(message,"%3.0fV",volts);
    OLED.print(message);
   }

  void displayConsole(){
    if (consoleRotate >= 200){ consoleIndex++; consoleRotate=0;}
    else consoleRotate++;
    if (consoleIndex >= 5) consoleIndex = 0;
    while (bottomLeftButton.isPressed() == true && topLeftButton.isPressed() == true){
    OLED.clearDisplay(); OLED.drawBitmap(0, 0,  STlogo, 128, 64, 1); OLED.display();}
    if (strcmp(consoleMessage[consoleIndex], "")==0){ consoleIndex++;}
    OLED.setCursor(0,56);
    OLED.setTextSize(1);
    OLED.setTextColor(WHITE);
    OLED.print(consoleMessage[consoleIndex]);
    }


  void led0(float reading, float threshold, bool boob){ 
    if (boob == true) leds[0] = CRGB::Yellow; 
    else if (driveActive == true) leds[0] = CRGB::Green;
    if (nannyMode == 1) leds[0] = CRGB::Pink;
    else if (nannyMode == 2) leds[0] = CRGB::Orange;
    else if (nannyMode == 3) leds[0] = CRGB::Blue;
    else leds[0] = CRGB::Black;
  }

  
  //AMS light
  void led1(float reading, float threshold, bool boob){ 
    if (boob == true){FastLED.setBrightness(255); leds[1] = CRGB::Red;}
   // else if (reading > threshold) leds[1] = CRGB::Green;
    else leds[1] = CRGB::Black;
  }


  //IMD light
  void led2(float reading, float threshold, bool boob){ 
    if (boob == true){FastLED.setBrightness(255); leds[2] = CRGB::Red;}
    //else if (reading > threshold) leds[2] = CRGB::Yellow;
    else leds[2] = CRGB::Black;
  }

  //This light is flaky
  void led3(float reading, float threshold, bool nanny){ 
    if (nanny == true) leds[3] = CRGB::Pink;
   
    else leds[3] = CRGB::Black;
  }

 
   void CanRXISR(){
        CANFrame tmp = CANFrame();
        byte intf = CAN.ReadRegister(CAN.CANINTF, 1);
        if ((intf & 0x01) > 0)
        {
                tmp = CAN.Read(0);
        //        Serial.print("INT 1:  ");
        //       Serial.print("GOT 1-0:   ");
        //     Serial.println(tmp.Data[0]);
        }
        if ((intf & 0x02) > 0)
        {
                tmp = CAN.Read(1);
        //        Serial.print("INT 1:  ");
        //        Serial.print("GOT 1-1:   ");
        }
        //Serial.println(tmp.Print());
       processFrame(tmp);
  }


  void processFrame(CANFrame f){
    //Serial.println(f.Print());
    if (f.ID == 0x00){
      Serial.print("GOT IT!!!!");
    }
    if (f.ID == 0x20){
      throttleOneRaw = f.Data[0];
      throttleOneRaw <<= 8;
      throttleOneRaw |= f.Data[1];
      throttleOneF = (float)(throttleOneRaw-256)/18.3;
      
      throttleTwoRaw = f.Data[2];
      throttleTwoRaw <<= 8;
      throttleTwoRaw |= f.Data[3];
      throttleTwoF = (float)(throttleTwoRaw-256)/18.3;
      throttleDiff = abs(throttleOneF-throttleTwoF);
      //Serial.println(throttleDiff);
      
      
      brakePressureFrontRaw = f.Data[4];
      brakePressureFrontRaw <<= 8;
      brakePressureFrontRaw |= f.Data[5];
      brakePressureFrontF = (float)(brakePressureFrontRaw-256)/10;
      //Serial.println(brakePressureFrontF);
      if (brakePressureFrontF >= 13){brakePressureBool = true;}
      else
      brakePressureBool = false;
      if (brakePressureFrontF >= brakeMax){brakeMax = brakePressureFrontF;}
      
      brakePressureRearRaw = f.Data[6];
      brakePressureRearRaw <<= 8;
      brakePressureRearRaw |= f.Data[7];
      brakePressureRearF = (float)(brakePressureRearRaw-256)/10;
      
      //Serial.println(brakePressureRearRaw);
    }
    if (f.ID == 0x28){
      uint8_t dataByte = f.Data[0]; 
      //Serial.println(f.Print());
        
        // STATUS Frame Generation:
        // Byte 0:
        //   |   |   |   |   | amsFault | imdFault | starting | driving |
        //   | 7 | 6 | 5 | 4 |    5     |    2     |    1     |    0    |
        //
        // Byte 1: HVSOC*10 (upper nibble)
        // Byte 2: HVSOC*10 (lower nibble)
        // Byte 3: LVSOC*10 (upper nibble)
        // Byte 4: LVSOC*10 (lower nibble)
        // Byte 5: speed*10 (upper nibble) (signed)
        // Byte 6: speed*10 (lower nibble) (signed)

      dataByte >>=1;
      if ((dataByte & 1) == 1){  driveActive = true;}
      else driveActive = false;
      dataByte >>=1;
      if ((dataByte & 1) == 1){  startActive = true;}
      else startActive = false;
      dataByte >>=1;
      if ((dataByte & 1) == 1){  IMDfault = true;}
      else IMDfault = false;
      dataByte >>=1;
      if ((dataByte & 1) == 1){  AMSfault = true;}
      else AMSfault = false;
      dataByte >>=1;
      if ((dataByte & 1) == 1){  shutdownActive = false;}
      else shutdownActive = true;
    
      LVSOC = f.Data[3]; 
      LVSOC <<= 8;
      LVSOC |= f.Data[4];
      LVSOCF = 0.0;//no LV sent, so set to zero for now
      //LVSOCF = (float)LVSOC/10;
      //Serial.println(LVSOCF);
      //Serial.println("low");
//      HVSOC = f.Data[1];
//      HVSOC <<= 8;
//      HVSOC |= f.Data[2];
//      HVSOCF = (float)HVSOC/10;

      carSpeed = f.Data[5];
      carSpeed <<= 8;
      carSpeed |= f.Data[6];
      //Serial.println(carSpeed);
      //sprintf(consoleMessage[1],"%d",f.Data[6]);
      
//      Serial.println("Got frame!");
//      Serial.println(f.Data[0]);
//      Serial.println(f.Data[1]);
//      Serial.println(f.Data[2]);
//      Serial.println(f.Data[3]);
//      Serial.println(f.Data[4]);
//      Serial.println(f.Data[5]);
//      Serial.println(f.Data[6]);
      carSpeedF = double(carSpeed)/10;
      dataByte = f.Data[7];
      if (dataByte & 1 == 1) { driveForward = true;}
      else driveForward = false;
      dataByte >>= 1;
      nannyMode = dataByte;
    }
    if (f.ID == 0x29){
    MCTemp = f.Data[0];
      MCTemp <<= 8;
      MCTemp |= f.Data[1];
      MCTempF = (float)MCTemp/10;
      //Serial.print("rinehart Temp:");
      //Serial.println(MCTempF);
      motorTemp = f.Data[2];
      motorTemp <<= 8;
      motorTemp |= f.Data[3];
      motorTempF = (float)motorTemp/10;
      //Serial.print("motor Temp:");
      //Serial.println(motorTempF); 
      batteryTemp = f.Data[4];
      batteryTemp <<= 8;
      batteryTemp |= f.Data[5];
      batteryTempF = (float)(batteryTemp)/10;
      //Serial.print("battery Temp:");
      //Serial.println(batteryTempF);
    }

    if (f.ID == 0x41){
      
      maxCellTemp = f.Data[1];
      maxCellTemp <<= 8;
      maxCellTemp |= f.Data[2];
      maxCellTempF = (float) maxCellTemp/10;
    //  Serial.print("max Cell Temp:");
      //Serial.println(maxCellTempF);
      
//      minCellTemp = f.Data[1];
//      minCellTemp <<= 8;
//      minCellTemp |= f.Data[2];
//      
      
      HVSOC = f.Data[5];
      HVSOC <<= 8;
      HVSOC |= f.Data[6];
      HVSOCF = (float)HVSOC/10;      
    }

    if (f.ID == 0x42){
      packVoltage = f.Data[0];
      packVoltage <<= 8;
      packVoltage |= f.Data[1];
      packVoltageF = (float)packVoltage / 10;
     // Serial.print("pack");
     // Serial.println(packVoltageF);
//
//      availableVoltage = f.Data[2];
//      availableVoltage <<= 8;
//      availableCellVoltage |= f.Data[3];
//      availableVoltageF = (float)availableVoltage / 1000;
//
      maxCellVoltage = f.Data[4];
      maxCellVoltage <<= 8;
      maxCellVoltage |= f.Data[5];
      maxCellVoltageF = (float)maxCellVoltage / 1000;
      
      minCellVoltage = f.Data[6];
      minCellVoltage <<= 8;
      minCellVoltage |= f.Data[7];
      minCellVoltageF = (float)minCellVoltage / 1000;
     // Serial.print("min cell: ");
     // Serial.println(minCellVoltage);  
    }
    if (f.ID == 0x43){
      packCurrent = f.Data[6];
      packCurrent <<= 8;
      packCurrent |= f.Data[7];
      packCurrentF = (float)packCurrent / 10;
      //Serial.print("packCurrent");
      //Serial.println(packCurrentF);
    }
//      sprintf(consoleMessage[1],"%d datas",f.Data[0]);
  }
  
  
  uint8_t buttonByte(){
    uint8_t buf = 0;
    if (bottomLeftButton.isPressed() == 1){
      buf |= 1;
    }
    buf <<= 1;
    if (topLeftButton.isPressed() == 1){
      buf |= 1;
    }
    buf <<= 1;
    if (topRightButton.isPressed() == 1){
      buf |= 1;
    }
    buf <<= 1;
    if (bottomRightButton.isPressed() == 1){
      buf |= 1;
    }
   // Serial.println(buf,BIN);
   return buf;
  }


      // This sends the button status over CAN ... for now
  void frameHandler(){
//    bool tmp;
//    CANFrame f = CANFrame(CAN.StdIDToExt(0x24),false,buttonByte(),0,0,0,0,0,0,0);
    CANFrame f = CANFrame(0x24,false,buttonByte(),0,0,0,0,0,0,0);//std frame
    
    CAN.Send(f,0);
    //Serial.println("send");
   //tmp = CAN.Send(f,0);
   // return tmp;
  }


void dashSwitchTop(){
  if (topLeftButton.uniquePress() == true){ dash++;}
    if (dash > dashCount){dash = 1;}
}

void dashSwitchBottom(){
  if (bottomLeftButton.uniquePress() == true){ dash--;}
    if (dash <= 0){dash = dashCount;}
}


void bigGauge(uint8_t xPos, uint8_t yPos, uint8_t r, float input){
      input = (input/100*1.72)+.7;
      OLED.drawCircle(xPos,yPos,r,WHITE);
      OLED.drawCircle(xPos,yPos,r/1.8,WHITE);
      OLED.drawLine(xPos,yPos,cos(input)*r + xPos,sin(input)*(-r) + yPos,WHITE);
  }
//      OLED.setTextColor(WHITE);
//      OLED.print(int(gIN));


void galaxyGauge(float input){
     input = (input/100*3.68)+.55;
     OLED.fillTriangle(60,75,68,75,(-cos(input))*45+64,sin(input)*-45+75,WHITE);
     OLED.drawLine(64,75,7,40,WHITE);
     OLED.drawLine(64,75,26,40,WHITE);
     OLED.drawLine(64,75,45,40,WHITE);
     OLED.drawLine(64,75,64,40,WHITE);
     OLED.drawLine(64,75,83,40,WHITE);
     OLED.drawLine(64,75,102,40,WHITE);
     OLED.drawLine(64,75,121,40,WHITE);
     OLED.setCursor(0,32);
     OLED.print("0");
     OLED.setCursor(18,32);
     OLED.print("10");
     OLED.setCursor(40,32);
     OLED.print("20");
     OLED.setCursor(59,32);
     OLED.print("30");
     OLED.setCursor(78,32);
     OLED.print("40");
     OLED.setCursor(97,32);
     OLED.print("50");
     OLED.setCursor(115,32);
     OLED.print("60"); 
}


void lamboArrowRight(float input){
  input = (input/100*1.04) + 2.62;
  int arrowLength = 40;
  float angle = .05;
  OLED.fillTriangle(-cos(input)*60+63,sin(input)*60+31,-cos(input+angle)*arrowLength+63,sin(input+angle)*arrowLength+31,-cos(input-angle)*arrowLength+63,sin(input-angle)*arrowLength+31,WHITE);
}


void lamboArrowLeft(float input){
  input = (input/100*1.04) + 5.76;
  int arrowLength = 40;
  float angle = .05;
  OLED.fillTriangle(-cos(input)*60+65,-sin(input)*60+31,-cos(input+angle)*arrowLength+65,-sin(input+angle)*arrowLength+31,-cos(input-angle)*arrowLength+65,-sin(input-angle)*arrowLength+31,WHITE);
}


void lamboGauge(double input){
  invgauge(64,31,31,input);// x position, y position, radius, float percentage
  OLED.drawCircle(64,31,63,WHITE);//vTMGauge(121,0,7,53,gIN);// x position, y position, width, length, float percentage
  OLED.drawFastHLine(33,31,5,WHITE);
//  OLED.drawLine(37,52,64,31,WHITE);
  OLED.drawFastHLine(91,31,5,WHITE);
  OLED.drawLine(38,47,41,45,WHITE);
  OLED.drawLine(54,61,56,57,WHITE);
  OLED.drawLine(74,61,72,57,WHITE);
  OLED.drawLine(90,47,87,45,WHITE);
}


void shuttleTemp(float a, float b, float c){
  vTMGauge(0,7,7,50,a);//Temp 1
  OLED.setCursor(1,0);
  OLED.print("1");
  vTMGauge(8,7,7,50,b);//Temp 2
  OLED.setCursor(9,0);
  OLED.print("2");
  vTMGauge(16,7,7,50,c);//Temp 3
  OLED.setCursor(17,0);
  OLED.print("B");
  OLED.drawFastHLine(16,57-(brakeMax/2),8,WHITE);//max Arrow
  OLED.drawFastVLine(24,56-(brakeMax/2),3,WHITE);
  OLED.drawFastHLine(16,40,7,WHITE);//25% Arrow
 // OLED.drawFastVLine(24,39,3,WHITE);
  
}
void shuttleTemp2(float a, float b, float c){
  vTMGauge(0,7,7,50,a);//Temp 1
  OLED.setCursor(1,0);
  OLED.print("R");
  vTMGauge(8,7,7,50,b);//Temp 2
  OLED.setCursor(9,0);
  OLED.print("M");
  vTMGauge(16,7,7,50,c/.6);//Temp 3
  OLED.setCursor(17,0);
  OLED.print("B");
}


void shuttleVolt(float hvsoc, float lvsoc, float tbd){
    SOCmeter(26,0,78,7,hvsoc,'H');// x position, y position, width, length, float percentage
    SOCmeter(26,8,78,7,lvsoc,'L');
    voltmeter(26,16,78,7,tbd);
//  voltmeter(26,0,78,7,(gIN/100)*1.2+3);// x position, y position, width, length, float percentage
//  voltmeter(26,8,78,7,(gIN/100)*1.2+3);
//  voltmeter(26,16,78,7,(gIN/100)*1.2+3);
}


void shuttleVolt2(float hvsoc, float lvsoc, float tbd){
    voltmeter2(26,0,78,7,hvsoc);
    voltmeter(26,8,78,7,lvsoc);
    voltmeter(26,16,78,7,tbd);
//  voltmeter(26,0,78,7,(gIN/100)*1.2+3);// x position, y position, width, length, float percentage
//  voltmeter(26,8,78,7,(gIN/100)*1.2+3);
//  voltmeter(26,16,78,7,(gIN/100)*1.2+3);
}


void shuttleThrottle(float throttle){
  hTMGauge(26,32,102,25,throttle);
  if (throttle > 25){
    OLED.fillTriangle(102,31,97,26,107,26,WHITE);
  }
  else {
  OLED.drawTriangle(102,31,97,26,107,26,WHITE);
  }
  idiotLiteShuttle(115,44,10,brakePressureBool);

  }   

void gameOver(){
  OLED.clearDisplay();
  OLED.setCursor(0,0);
  OLED.setTextSize(2);
  OLED.setTextColor(WHITE);
//  OLED.print("GAME OVER");
  OLED.print("Throttle Plausibility Detected");
  OLED.display();
}

  


